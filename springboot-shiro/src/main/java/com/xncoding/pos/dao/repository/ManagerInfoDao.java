package com.xncoding.pos.dao.repository;

import com.xncoding.pos.common.dao.repository.ManagerMapper;
import com.xncoding.pos.dao.entity.ManagerInfo;

/**
 * Description  :
 */
public interface ManagerInfoDao extends ManagerMapper {
    ManagerInfo findByUsername(String username);
}
